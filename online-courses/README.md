# ONLINE COURSES

## ALGORITHM

* [EDX-Algorithms: Design and Analysis](https://www.edx.org/course/algorithms-design-and-analysis)
* [Coursera-Data Structures and Algorithms Specialization](https://www.coursera.org/specializations/data-structures-algorithms) - 49$ per month.
* [EDX-CS50's Introduction to Computer Science](https://www.edx.org/course/cs50s-introduction-to-computer-science)
* [YOUTUBE CS50](https://www.youtube.com/channel/UCcabW7890RKJzL968QWEykA)
* [COURSERA-ALGORITHM-1](https://www.coursera.org/learn/algorithms-part1?ranMID=40328&ranEAID=SAyYsTvLiGQ&ranSiteID=SAyYsTvLiGQ-H68xzQZVg1nOl0Q0Xd0DlA&siteID=SAyYsTvLiGQ-H68xzQZVg1nOl0Q0Xd0DlA&utm_content=10&utm_medium=partners&utm_source=linkshare&utm_campaign=SAyYsTvLiGQ) It's free
## System Design

1. [Educative - Grokking the System Design Interview](https://www.educative.io/courses/grokking-the-system-design-interview) around ~$10.39 per month. It has no videos, just texts

---

### OTHER

- [classcentral](https://www.classcentral.com/collection/ivy-league-moocs?fbclid=IwAR19cNMRXbCmUcAqrZRtECOnXarye_vtEYOK1AUlGRJlMD3BxsgZWOUOEVI&subject=cs) - display all free courses from IVY LEAGUE Colleges
