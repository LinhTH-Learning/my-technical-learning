# Specification

> the pattern is integrated inside the Spring Framework

## Interface

```Java
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;

public interface Specification<T> extends Serializable {
    long serialVersionUID = 1L;

    static <T> Specification<T> where(Specification<T> spec) {
        return spec == null ? (root, query, builder) -> null : spec;
    }

    default Specification<T> and(Specification<T> otherSpec) {
        return SpecificationComposition.composed(this, otherSpec, (CriteriaBuilder::and));
    }

    default Specification<T> or(Specification<T> otherSpec) {
        return SpecificationComposition.composed(this, otherSpec, (CriteriaBuilder::or));
    }

    Predicate toPredicate(Root<T> root, CriteriaQuery query, CriteriaBuilder builder);
```

```Java
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.io.Serializable;

public class SpecificationComposition {

    interface Combiner extends Serializable {
        Predicate combine(CriteriaBuilder builder, Predicate thisPredicate, Predicate otherPredicate);
    }

    static <T> Specification<T> composed(Specification<T> thisSpec, Specification<T> otherSpec, Combiner combiner) {
        return (root, query, builder) -> {
            Predicate thisPredicate = thisSpec == null ? null : thisSpec.toPredicate(root, query, builder);
            Predicate otherPredicate = otherSpec == null ? null : otherSpec.toPredicate(root, query, builder);

            if (thisPredicate == null) {
                return otherPredicate;
            }

            return otherPredicate == null ? thisPredicate : combiner.combine(builder, thisPredicate, otherPredicate);
        };
    }
}
```

## Abstract Repository

```Java
import com.google.common.collect.Lists;
import org.hibernate.Session;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T extends Serializable, ID extends Serializable> {

    private final static int MAX_NUMBER = 1000;

    @Inject
    private EntityManager entityManager;

    public T getOne(final ID id) {
        return entityManager.find(getPersistenceClass(), id);
    }

    public T save(final T object) {
        getSession().save(object);
        return object;
    }

    public T saveOrUpdate(final T object) {
        getSession().saveOrUpdate(object);
        return object;
    }

    public List<T> findAll(Specification<T> specification) {
        if (specification == null) {
            return Collections.emptyList();
        }

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = criteriaBuilder.createQuery(getPersistenceClass());
        Root<T> root = query.from(getPersistenceClass());

        Predicate predicate = specification.toPredicate(root, query, criteriaBuilder);

        query.where(predicate);

        return entityManager.createQuery(query).getResultList();
    }

    protected abstract Class<T> getPersistenceClass();

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * This method is created to deal with the problem of long In list (> 1000 elements)
     * Solution: break the condition into many OR conditions
     */
    protected <D> Predicate adjustInClausePredicate(Expression<List<D>> path, List<D> list) {
        Predicate result;
        if (list.size() > MAX_NUMBER) {
            List<List<D>> subs = Lists.partition(list, MAX_NUMBER);
            Predicate[] inPredicates = subs.stream().map(sub -> getEntityManager().getCriteriaBuilder().in(path).value(sub)).collect(Collectors.toList()).toArray(new Predicate[]{});
            result = getEntityManager().getCriteriaBuilder().or(inPredicates);
        } else {
            result = getEntityManager().getCriteriaBuilder().in(path).value(list);
        }
        return result;
    }

    private Session getSession() {
        return entityManager.unwrap(Session.class);
    }
}
```

## Example

```Java
    private static Specification<Person> relatedToPerson(String personName) {
        return (root, query, builder) -> {
            Subquery<UUID> subQuery = query.subquery(UUID.class);
            Root<OtherPerson> subRoot = subQuery.from(OtherPerson.class);
            ListJoin<Person, OtherPerson> subPersonNames = subRoot.joinList(NAME_OF_ATTRIBUTE, JoinType.INNER);
            subQuery.select(subRoot.get(NAME_OF_ATTRIBUTE));
            subQuery.where(builder.equal(subPersonNames.get(NAME_OF_ATTRIBUTE).get(NAME_OF_ATTRIBUTE), personName));

            return builder.in(root.get(NAME_OF_ATTRIBUTE)).value(subQuery);
        };
    }
```

```Java
        repository.findAll(
            where(relatedToPerson("LinhTH"))
        );
```
